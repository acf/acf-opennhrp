<% local data, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% htmlviewfunctions.displayitem(data.value.status) %>
<% htmlviewfunctions.displayitemstart() %>
<%= html.html_escape(data.value.peers_list.label) %>
<% htmlviewfunctions.displayitemmiddle() %>
<%
	local found
	for intf,addresses in pairs(data.value.peers_list.value) do
		found = true
%>
		<table style='margin-bottom:10px;'>
		<tr><td style='font-weight:bold;border:none;'><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/places/network-server.png' width='16' height='16' alt> <%= html.html_escape(intf) %></td><td style='border:none;'></td></tr>
<%		for i,entries in ipairs(addresses) do
			io.write("<tr style='padding-bottom:10px;'><td width='150px' style='font-weight:bold;padding-left:20px;border:none;'><img src='"..html.html_escape(page_info.wwwprefix..page_info.staticdir).."/tango/16x16/status/")

			if entries.Type and (entries.Type.value == "negative") then
				io.write("network-offline")
			elseif (not entries.Flags) or (entries.Flags.value == "up") then
				io.write("network-idle")
			elseif entries.Flags and (entries.Flags.value == "used up") then
				io.write("network-transmit-receive")
			else
				io.write("network-error")
			end
			io.write(".png' width='16' height='16' title='" .. html.html_escape(entries.Type.descr) .. "'> " .. html.html_escape(entries["Protocol-Address"].value) .. "</td><td style='font-weight:bold;border:none;'></td></tr>\n")

    			for j,entry in pairs(entries) do
				if  j ~= "Protocol-Address" then
					io.write("<tr><td style='font-weight:bold;padding-left:40px;border:none;'>"..html.html_escape(entry.label).."</td><td style='border:none;'>"..html.html_escape(entry.value))
					if entry.descr then
						io.write(" <i>(" .. html.html_escape(entry.descr) .. ")</i>")
					end
					io.write("</td></tr>\n")
				end
			end
		end
		io.write("\t\t\t</table>")
	end
	if not found then io.write("No peers found\n") end
%>
<% htmlviewfunctions.displayitemend() %>
<% htmlviewfunctions.displaysectionend(header_level) %>
