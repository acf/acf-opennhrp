<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
        if (typeof jQuery == 'undefined') {
                document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	function enabledisable(){
		if ($("#type").val() == "NHRP Enabled") {
			$("#map").parents(".item").show();
			$("#dynamic-map").parents(".item").show();
		} else {
			$("#map").parents(".item").hide();
			$("#dynamic-map").parents(".item").hide();
		}
	}
	$(function(){
		$("#type").change(enabledisable);
		enabledisable();
	});
</script>

<% htmlviewfunctions.displayitem(form, page_info) %>
