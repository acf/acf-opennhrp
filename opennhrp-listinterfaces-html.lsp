<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"editinterface"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Interface</th>
		<th>Type</th>
		<th>Comment</th>
	</tr>
</thead><tbody>
<% local interface = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,intf in ipairs(view.value) do %>
	<tr>
		<td>
		<% interface.value = intf.interface %>
		<% htmlviewfunctions.displayitem(cfe({type="link", value={interface=interface, redir=redir}, label="", option="Edit", action="editinterface"}), page_info, -1) %>
		</td>
		<td><%= html.html_escape(intf.interface) %></td>
		<td><%= html.html_escape(intf.type) %></td>
		<td><P class="error"><%= string.gsub(html.html_escape(intf.errtxt), "\n", "<br/>") %></P></td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>
