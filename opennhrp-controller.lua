local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.show(self)
	return self.model.getshowreport()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listinterfaces(self)
	return self.model.listinterfaces(self)
end

function mymodule.editinterface(self)
	return self.handle_form(self, function() return self.model.getinterfacedetails(self.clientdata.interface) end, self.model.updateinterfacedetails, self.clientdata, "Save", "Edit Interface Config", "Interface Configuration Set")
end

function mymodule.expert (self)
	return self.handle_form(self, self.model.getconfigfile, self.model.setconfigfile, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

return mymodule
